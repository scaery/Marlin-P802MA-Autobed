
from os.path import join
Import("env", "projenv")

PROGNAME = ("woot")

# Custom BIN from ELF
env.AddPostAction(
    "$BUILD_DIR/${PROGNAME}.hex",
    env.VerboseAction(" ".join([
                "$OBJCOPY",
                "-O",
                "binary",
                "$TARGET",
                "${PROGNAME}.hex"
            ]), "Building $TARGET"))