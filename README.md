# Marlin for Tronxy P802MA Firmware
Customized firmware for Tronxy P802MA 3D printer with autobed leveling (Melzi LCD2004 + 5 keys)

Bed Size 220x220

X Endstop left

Y Endstop back

Z LJC18A3-B-Z/BX

## Features

* Hotend PID tuning for 501 temperature sensor
* Bed Auto Leveling with LJC18A3-B-Z/BX (rear behind)
* Customized offsets for Flexplate or Glass

Thanks to:

https://marlinfw.org and the community behind https://github.com/MarlinFirmware/Marlin